import os
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Key, Group, KeyChord, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List  # noqa: F401
from libqtile.dgroups import simple_key_binder

mod = "mod4"
myTerm = "kitty"
myBrowser = "brave"

#Binds - Shortcuts
keys = [
    Key([mod], "Return",
        lazy.spawn('kitty'),
        ),
    Key([mod, "shift"], "Return",
        lazy.spawn("rofi -show drun"),
        ),
    Key([mod, "shift"], "s",
        lazy.spawn("pavucontrol"),
        ),
    Key([mod, "shift"], "w",
        lazy.spawn("google-chrome"),
        ),
    Key([mod], "w",
        lazy.spawn("brave"),
        ),
    Key([mod], "r",
        lazy.spawn("kitty -e ranger"),
        ),
    Key([mod], "e",
        lazy.spawn("nvim"),
        ),
    Key([mod, "shift"], "e",
        lazy.spawn("code"),
        ),
    Key([mod], "Tab",
        lazy.next_layout(),
        desc='Toggle through layouts'
        ),
    Key([mod], "q",
        lazy.window.kill(),
        desc='Kill active window'
        ),
    Key([mod, "shift"], "r",
        lazy.restart(),
        desc='Restart Qtile'
        ),
    Key([mod, "shift"], "x",
        lazy.shutdown(),
        desc='Shutdown Qtile'
        ),
    Key([mod], "right",
        lazy.next_screen(),
        desc='Move focus to next monitor'
        ),
    Key([mod], "left",
        lazy.prev_screen(),
        desc='Move focus to prev monitor'
        ),
    Key([mod], "down",
        lazy.layout.down(),
        desc='Move focus down in current stack pane'
        ),
    Key([mod], "up",
        lazy.layout.up(),
        desc='Move focus up in current stack pane'
        ),
    Key([mod, "shift"], "down",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "up",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "right",
        lazy.layout.shuffle_right(),
        lazy.layout.section_right(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "left",
        lazy.layout.shuffle_left(),
        lazy.layout.section_left(),
        desc='Move windows up in current stack'
        ),
    Key([mod, "control"], "right",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
        ),
    Key([mod, "control"], "left",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'
        ),
    Key([mod, "control"], "down",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
        ),
    Key([mod, "control"], "up",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'
        ),
    Key([mod], "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'
        ),
    Key([mod], "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'
        ),
    Key([mod, "shift"], "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'
        ),
    Key([mod], "f",
        lazy.window.toggle_fullscreen(),
        desc='toggle fullscreen'
        ),
    Key([mod, "shift"], "Tab",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'
        ),
    Key([mod], "space",
        lazy.layout.next(),
        desc='Switch window focus to other pane(s) of stack'
        ),
    Key([mod, "shift"], "space",
        lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'
        ),
]

dgroups_key_binder = simple_key_binder("mod4")

colors = [["#282A2E"],	#0	normal black
		["#ff6c6b"],	#1	normal red
		["#98be65"],	#2	normal green
		["#da8548"],	#3	normal yellow
		["#51afef"],	#4	normal blue
		["#c678dd"],	#5	normal magenta
		["#46d9ff"],	#6	normal cyan
		["#bbc2cf"],	#7	normal white
		["#373B41"],	#8	bright black
		["#ff6655"],	#9	bright red
		["#99bb66"],	#10	bright green
		["#ECBE7B"],	#11	bright yellow
		["#56BBFF"],	#12	bright blue
		["#B294BB"],	#13	bright magenta
		["#46D9FF"],	#14	bright cyan
		["#DFDFDF"]]	#15	bright white

layout_theme = {"border_width": 3,
                "margin": 4,
                "border_focus": "#51afef",
                "border_normal": "#363537",
                "master_length": 3,
                # "ratio": 1,
                }

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Matrix(**layout_theme)]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="JetBrainsMonoNL NFM Bold",
    fontsize=15,
    background=colors[2])

extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
        widget.Systray(
            background=colors[8],
            padding=5
        ),
        widget.Spacer(
            linewidth=0,
            padding=12,
            foreground=colors[0],
            background=colors[0]
        ),
        widget.GroupBox(
            font = "delugia pl bold",
            fontsize =18,
            margin_y=3,
            margin_x=0,
            padding_y=5,
            padding_x=8,
            borderwidth=3,
            active=colors[15],
            inactive='#676B72',
            rounded=False,
            highlight_color=colors[0],
            highlight_method="block",
            this_current_screen_border=colors[4],
            this_screen_border=colors[4],
            other_current_screen_border=colors[8],
            other_screen_border=colors[8],
            foreground=colors[2],
            background=colors[0]
        ),
        widget.Spacer(
            length=bar.STRETCH,
            background=colors[0],
        ),
        widget.Volume(
            step=5,
            fmt=" {}",
            limit_max_volume=True,
            foreground=colors[4],
            background=colors[8],
            scroll_delay=0,
            padding=8,
            volume_up_command="pactl set-sink-volume @DEFAULT_SINK@ +5%",
		    volume_down_command="pactl set-sink-volume @DEFAULT_SINK@ -5%",
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("pavucontrol")}      
        ),
        widget.CPU(
            foreground=colors[6],
            background=colors[8],
            padding=8,
            format='CPU {load_percent}%',
        ),
        widget.Memory(
            foreground=colors[2],
            background=colors[8],
            padding=8,
            mouse_callbacks={
                'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e btop')},
            format='MEM {MemPercent}%',
            measure_mem='G',
        ),
        widget.DF(
            foreground=colors[5],
            background=colors[8],
            padding=8,
            visible_on_warn=False,
            measure='G',
            #format='{f}g {r:.0f}%',
            format='disk {r:.0f}%',
        ),
        widget.ThermalSensor(
            foreground=colors[1],
            background=colors[8],
            padding=8,  
            tag_sensor='CPU', #run sensors in terminal
        ),
        widget.Clock(
            font='JetBrainsMonoNL NFM Bold Italic',
            foreground=colors[3],
            background=colors[8],
            padding=10,
            format="%A, %B %d - %H:%M ",
        ),
    ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[0]
    return widgets_screen2


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=28, border_width=[5, 5, 5, 5], border_color=["272A2D", "272A2D", "272A2D", "272A2D"])),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=28, border_width=[6, 6, 6, 6], border_color=["272A2D", "272A2D", "272A2D", "272A2D"]))]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()


def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)


def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())]

dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False

groups = [
    Group("1", layout='monadtall'),
    Group("2", layout='monadtall'),
    Group("3", layout='monadtall'),
    Group("4", layout='monadtall'),
    Group("5", layout='monadtall'),
    Group("6", layout='monadtall'),
    Group("7", layout='monadtall'),
    Group("8", layout='monadtall'),
    Group("9", layout='monadtall'),
    Group("0", layout='floating')
]
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),      # tastyworks exit box
    Match(title='Qalculate!'),        # qalculate-gtk
    Match(wm_class='kdenlive'),       # kdenlive
    Match(wm_class='pinentry-gtk-2'),  # GPG key password entry
    Match(title='archlinux-tweak-tool'),
    Match(wm_class='Celluloid'),
    Match(wm_class='nvidia-settings'),
    Match(wm_class='quake'),
    Match(wm_class='xfce4-taskmanager'),
    Match(wm_class='archlinux-tweak-tool'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False


#@hook.subscribe.client_new
#@hook.subscribe.startup_once
#def start_once():
#    home = os.path.expanduser('~')
#    subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.client_new
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])




wmname = "qtile"
