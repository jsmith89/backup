import os
import socket
import subprocess
from libqtile import layout, bar, widget, hook, qtile
from libqtile.config import Click, Drag, Key, Group, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List  # noqa: F401
from libqtile.dgroups import simple_key_binder

colors = [
    "#282A2E", "#ff6c6b", "#98be65", "#da8548",
    "#51afef", "#c678dd", "#46d9ff", "#bbc2cf",
    "#373B41", "#ff6655", "#99bb66", "#ECBE7B",
    "#56BBFF", "#B294BB", "#46D9FF", "#DFDFDF"
]

mod = "mod4"

keys = [
    Key([mod], "Return", lazy.spawn("kitty")),
    Key([mod, "shift"], "Return", lazy.spawn("rofi -show drun")),
    Key([mod, "shift"], "s", lazy.spawn("pavucontrol")),
    Key([mod, "shift"], "w", lazy.spawn("google-chrome")),
    Key([mod], "f1", lazy.spawn("thunar")),
    Key([mod], "w", lazy.spawn("brave")),
    Key([mod], "r", lazy.spawn("kitty" + " -e ranger")),
    Key([mod], "e", lazy.spawn("nvim")),
    Key([mod, "shift"], "e", lazy.spawn("vscodium")),
    Key([mod], "Tab", lazy.next_layout(),),
    Key([mod], "q", lazy.window.kill(),),
    Key([mod, "shift"], "r", lazy.restart(),),
    Key([mod, "shift"], "x", lazy.shutdown(),), 
    Key([mod], "right", lazy.next_screen(),),
    Key([mod], "left", lazy.prev_screen(),),
    Key([mod], "down", lazy.layout.down(),),
    Key([mod], "up", lazy.layout.up(),),
    Key([mod, "shift"], "down", lazy.layout.shuffle_down(), lazy.layout.section_down()),
    Key([mod, "shift"], "up", lazy.layout.shuffle_up(), lazy.layout.section_up()),
    Key([mod, "shift"], "right", lazy.layout.shuffle_right(), lazy.layout.section_right(),),
    Key([mod, "shift"], "left", lazy.layout.shuffle_left(), lazy.layout.section_left(),),
    Key([mod, "control"], "right", lazy.layout.shrink(), lazy.layout.decrease_nmaster(),),
    Key([mod, "control"], "left", lazy.layout.grow(), lazy.layout.increase_nmaster(),),
    Key([mod, "control"], "down", lazy.layout.shrink(), lazy.layout.decrease_nmaster(),),
    Key([mod, "control"], "up", lazy.layout.grow(), lazy.layout.increase_nmaster(),),
    Key([mod], "n", lazy.layout.normalize(),),
    Key([mod], "m", lazy.layout.maximize(),),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(),),
    Key([mod], "f", lazy.window.toggle_fullscreen(),),
    Key([mod, "shift"], "Tab", lazy.layout.rotate(),),
    Key([mod], "space", lazy.layout.next(),),
    Key([mod, "shift"], "space", lazy.layout.toggle_split(),),
]

dgroups_key_binder = simple_key_binder("mod4")

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
]

layout_theme = {
    "border_width": 3,
    "margin": 4,
    "border_focus": "#51afef",
    "border_normal": "#363537",
    "master_length": 3
}

widget_defaults = dict(
    font="JetBrainsMonoNL NFM Bold",
    fontsize=10,
    background=colors[8],
    padding=8
)

extension_defaults = widget_defaults.copy()

def init_widgets_list():
    return [
        widget.Systray(
            padding=10
        ),
        widget.Spacer(
            linewidth=0,
            foreground=colors[0],
            background=colors[0]
        ),
        widget.GroupBox(
            margin_y=3,
            margin_x=0,
            padding_y=5,
            padding_x=8,
            borderwidth=3,
            active=colors[15],
            inactive='#676B72',
            rounded=False,
            highlight_color=colors[0],
            highlight_method="block",
            this_current_screen_border=colors[4],
            this_screen_border=colors[4],
            other_current_screen_border=colors[8],
            other_screen_border=colors[8],
            foreground=colors[2],
            background=colors[0]
        ),
        widget.Spacer(
            length=bar.STRETCH,
            background=colors[0],
        ),
        widget.Volume(
            step=5,
            fmt=" {}",
            limit_max_volume=True,
            foreground=colors[4],
            scroll_delay=0,
            volume_up_command="pactl set-sink-volume @DEFAULT_SINK@ +5%",
            volume_down_command="pactl set-sink-volume @DEFAULT_SINK@ -5%",
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("pavucontrol")}      
        ),
        widget.CPU(
            foreground=colors[6],
            format='CPU {load_percent}%',
        ),
        widget.Memory(
            foreground=colors[2],
            mouse_callbacks={
                'Button1': lambda: qtile.cmd_spawn("kitty" + ' -e btop')},
            format='MEM {MemPercent}%',
            measure_mem='G',
        ),
        widget.DF(
            foreground=colors[5],
            visible_on_warn=False,
            measure='G',
            format='disk {r:.0f}%',
        ),
        widget.ThermalSensor(
            foreground=colors[1],
            tag_sensor='CPU',
        ),
        widget.Clock(
            font='JetBrainsMonoNL NFM Bold Italic',
            foreground=colors[3],
            format="%A, %B %d - %H:%M ",
        ),
    ]

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[0]
    del widgets_screen2[3:10]
    return widgets_screen2

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_screens():
    return [
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen1(),
                opacity=1.0,
                size=22,
            )
        ),
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen2(),
                opacity=1.0,
                size=22,
            )
        )
    ]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.current_window is not None:
        i = qtile.groups.index(qtile.current_group)
        qtile.current_window.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.current_window is not None:
        i = qtile.groups.index(qtile.current_group)
        qtile.current_window.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

dgroups_app_rules = []  # type: List

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Matrix(**layout_theme)
]

Groups = [
    Group("1", layout='monadtall'),
    Group("2", layout='monadtall'),
    Group("3", layout='monadtall'),
    Group("4", layout='monadtall'),
    Group("5", layout='monadtall'),
    Group("6", layout='monadtall'),
    Group("7", layout='monadtall'),
    Group("8", layout='monadtall'),
    Group("9", layout='monadtall'),
    Group("0", layout='floating')
]

floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),      # tastyworks exit box
    Match(title='Qalculate!'),        # qalculate-gtk
    Match(wm_class='kdenlive'),       # kdenlive
    Match(wm_class='pinentry-gtk-2'),  # GPG key password entry
    Match(title='quake'),
    Match(title='ezQuake-x86_64'),

    Match(wm_class='Celluloid'),
    Match(wm_class='nvidia-settings'),
    Match(wm_class='ezQuake-x86_64'),
    Match(wm_class='quake'),
    Match(wm_class='xfce4-taskmanager'),
    Match(wm_class='archlinux-tweak-tool'),
])

follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = False

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

@hook.subscribe.client_new

@hook.subscribe.startup_once
def autostart():
    import subprocess
    subprocess.Popen([home + '/.config/qtile/autostart.sh'])

wmname = "qtile"
