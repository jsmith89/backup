#!/bin/bash
echo 999 | sudo tee /sys/class/backlight/intel_backlight/brightness &
nm-applet &
nitrogen --restore &
flameshot &
caprine &
discord &
xrandr --output eDP-1 --off --output DP-1 --off --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --mode 2560x1440 --pos 0x0 --rotate normal

